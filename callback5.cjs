const getBoardInfoFromID=require('./callback1.cjs');
const getListFromID=require('./callback2.cjs');
const getCardFromListID=require('./callback3.cjs');

function getMindAndSpaceCard(boardID,callback)
{
   
    setTimeout(() =>
    {
        getBoardInfoFromID(boardID,(error,data) =>
        {
            if(error)
            {
                console.error(error)
            }else
            {
              const boardData=data;
                
                getListFromID(boardID,(error,data) =>
                {
                    if(error)
                    {
                        console.error(error)
                    }else
                    {
                        const listData=data;
                        let inputData= data.filter((item) =>
                        {
                            return item.name==='Mind'|| item.name==='Space';
                        });
                        let mindCard;
                            getCardFromListID(inputData[0].id,(error,data) =>
                            {
                                if(error)
                                {
                                    console.error(error);
                                }else{
                                   mindCard=data;                            
                                }
                            })

                            getCardFromListID(inputData[1].id,(error,data) =>
                            {
                                if(error)
                                {
                                    console.error(error);
                                }else{
                                   const SpaceCard=data;
                                    callback(null,boardData,listData,mindCard,SpaceCard)
                                }
                            })

                    }
        
                })
            }

        });
        
    }, 2 * 1000) 
}

module.exports=getMindAndSpaceCard;