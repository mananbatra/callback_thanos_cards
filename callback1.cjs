const fs=require('fs');
const path = require('path');
const filePath='boards.json';
const boardFilePath=path.join(__dirname,filePath);

function getBoardInfoFromID(boardID, callback)
{
    setTimeout(() =>
    {
        fs.readFile(boardFilePath,'utf8',(error , data)=>
        {
            if(error)
            {
                callback(error)
            }else{

                data = JSON.parse(data);
                const infoOnID= data.filter((items)=>
                {
                    return items.id === boardID;
                })
               // console.log(infoOnID);
                callback(null ,infoOnID);
            }
        })
    }, 2 * 1000)        // 2 sec timeout

}

module.exports=getBoardInfoFromID;

