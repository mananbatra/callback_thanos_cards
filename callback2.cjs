const fs=require('fs');
const path = require('path');
const filePath='lists.json';
const listFilePath=path.join(__dirname,filePath);

function getListFromID(boardID, callback)
{
    setTimeout(() =>
    {
        fs.readFile(listFilePath,'utf8',(error , data)=>
        {
            if(error)
            {
                callback(error)
            }else{

                data = JSON.parse(data);
                let keys=Object.keys(data);
                
                const infoOnID= keys.find((key)=>
                    {
                        return key === boardID;
                    })

                const result =data[infoOnID];
                callback(null ,result);
            }
        })
    }, 2 * 1000)        // 2 sec timeout

}

module.exports=getListFromID;

