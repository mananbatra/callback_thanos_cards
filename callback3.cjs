const fs=require('fs');
const path = require('path');
const filePath='cards.json';
const cardFilePath=path.join(__dirname,filePath);

function getCardFromListID(listID, callback)
{
    setTimeout(() =>
    {
        fs.readFile(cardFilePath,'utf8',(error , data)=>
        {
            if(error)
            {
                callback(error)
            }else{

                data = JSON.parse(data);
                let keys=Object.keys(data);
                
                const infoOnID= keys.find((key)=>
                    {
                        return key === listID;
                    })

                const result =data[infoOnID];
                callback(null ,result);
            }
        })
    }, 2 * 1000)        // 2 sec timeout

}

module.exports=getCardFromListID;

