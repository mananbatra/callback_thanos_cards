const getBoardInfoFromID=require('./callback1.cjs');
const getListFromID=require('./callback2.cjs');
const getCardFromListID=require('./callback3.cjs');

function getAllCardInfo(boardID)
{
   
    setTimeout(() =>
    {
        getBoardInfoFromID(boardID,(error,data) =>
        {
            if(error)
            {
                console.error(error)
            }else
            {
                console.log(data);
                
                getListFromID(boardID,(error,data) =>
                {
                    if(error)
                    {
                        console.error(error)
                    }else
                    {
                        console.log(data);
                        const listData=data;
                        listData.map((items) =>
                        {
                            getCardFromListID(items.id,(error,data) =>
                            {
                                if(error)
                                {
                                    console.error(error);
                                }else{
                                   console.log(items.name ,data);
                                }
                            })

                        })

                    }
        
                })
            }

        });
        
    }, 2 * 1000) 
}

module.exports=getAllCardInfo;