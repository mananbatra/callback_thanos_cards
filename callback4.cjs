const getBoardInfoFromID=require('./callback1.cjs');
const getListFromID=require('./callback2.cjs');
const getCardFromListID=require('./callback3.cjs');

function getInfoFromPreviousFunctions(boardID,callback)
{
   
    setTimeout(() =>
    {
        getBoardInfoFromID(boardID,(error,data) =>
        {
            if(error)
            {
                console.error(error)
            }else
            {
              const boardData=data;
                
                getListFromID(boardID,(error,data) =>
                {
                    if(error)
                    {
                        console.error(error)
                    }else
                    {
                        const listData=data;
                        let inputData= data.find((item) =>
                        {
                            return item.name==='Mind';
                        });

                        getCardFromListID(inputData.id,(error,data) =>
                        {
                            if(error)
                            {
                                console.error(error);
                            }else{
                               const cardData=data;
                                callback(null,boardData,listData,cardData)
                            }
                        })
                    }
        
                })
            }

        });
        
    }, 2 * 1000) 
}

module.exports=getInfoFromPreviousFunctions;