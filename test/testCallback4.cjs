const getInfoFromPreviousFunctions=require('../callback4.cjs');

function callback(error, boardData,listData,cardData)
{
    if(error)
    {
        console.log(error);
    }else{

        console.log('Problem 4: Thanos Mind Cards');
        console.log('Board:', boardData);
        console.log('Lists:', listData);
        console.log('Cards:', cardData);
    }
}

getInfoFromPreviousFunctions('mcu453ed',callback);