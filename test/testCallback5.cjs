const getMindAndSpaceCard=require('../callback5.cjs');

function callback(error, boardData,listData,mindCard,spaceCards)
{
    if(error)
    {
        console.log(error);
    }else{

        console.log('Problem 5: Thanos Mind and Space Cards');
        console.log('Board:', boardData);
        console.log('Lists:', listData);
        console.log('Mind Cards:', mindCard);
        console.log('Space Cards:', spaceCards);
    }
}

getMindAndSpaceCard('mcu453ed',callback);